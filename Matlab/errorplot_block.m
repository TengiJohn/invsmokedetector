function  errorplot_block ( )
filename=[pwd '\..\SmokeDetector\experiment\sMoky-1.txt'];
a1=load(filename);
a1=a1(1:2:length(a1))./a1(2:2:length(a1));

filename=[pwd '\..\SmokeDetector\experiment\waste-1.txt'];
a3=load(filename);
a3=a3(1:2:length(a3))./a3(2:2:length(a3));
a1=[a1;a3];

filename=[pwd '\..\SmokeDetector\experiment\cross-1.txt'];
a2=load(filename);
a2=a2(1:2:length(a2))./a2(2:2:length(a2));

t=0:0.03:3;
e1=zeros(1,length(t));
e2=zeros(1,length(t));

for i=1:length(t)
    e1(i)=sum(a1>t(i));
    e2(i)=sum(a2<t(i));
end

e1=e1/length(a1);
e2=e2/length(a2);

filename=[pwd '\..\SmokeDetector\experiment\sMoky.txt'];
a1=load(filename);
a1=a1(1:2:length(a1))./a1(2:2:length(a1));

filename=[pwd '\..\SmokeDetector\experiment\waste.txt'];
a3=load(filename);
a3=a3(1:2:length(a3))./a3(2:2:length(a3));
a1=[a1;a3];

filename=[pwd '\..\SmokeDetector\experiment\cross.txt'];
a2=load(filename);
a2=a2(1:2:length(a2))./a2(2:2:length(a2));

e11=zeros(1,length(t));
e22=zeros(1,length(t));
for i=1:length(t)
    e11(i)=sum(a1>t(i));
    e22(i)=sum(a2<t(i));
end

e11=e11/length(a1);
e22=e22/length(a2);

plot(t,e11,'.r',t,e22,'+r',t,e1,'.b',t,e2,'+b');
hold on;

end


