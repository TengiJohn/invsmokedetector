function detail=wavelet(pic,wavename)
%%%%%%%%%%%%%%%%%%%%%
% wavename='haar';
% src=imread('../smokedetector/sample/slpw.jpg');
% pic=double(rgb2gray(src));
%%%%%%%%%%%%%%%%%%%%%

[L_d,H_d,L_r,H_r]=wfilters(wavename);
width=size(pic,2);
height=size(pic,1);

reswidth=floor(width/2);
resheight=floor(height/2);
tempDetail=zeros(height,reswidth);
tempApprox=zeros(height,reswidth);

for i=1:height
    tempDetail(i,:)=dyaddown(conv(pic(i,:),H_d,'same'),0);
%     tempApprox(i,:)=dyaddown(conv(pic(i,:),L_d,'same'),0);
end

approx=zeros(resheight,reswidth);
vertical=zeros(resheight,reswidth);
detail=zeros(resheight,reswidth);
horizon=zeros(resheight,reswidth);

for i=1:reswidth
%     approx(:,i)=dyaddown(conv(tempApprox(:,i),L_d,'same'),0);
%     vertical(:,i)=dyaddown(conv(tempApprox(:,i),H_d,'same'),0);
    detail(:,i)=dyaddown(conv(tempDetail(:,i),H_d,'same'),0);
%     horizon(:,i)=dyaddown(conv(tempDetail(:,i),L_d,'same'),0);
end

% imshow(detail,[]);
end

