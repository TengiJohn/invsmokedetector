function disorder()
%meausre SEP/STP
minArea=100;

BW=getappdata(0,'colorfilter_image');
L=bwlabeln(BW);

S = regionprops(L,'Area');

BW=ismember(L,find([S.Area]>minArea));

S = regionprops(BW,'Area');
box=regionprops(BW,'BoundingBox');
boundaries=regionprops(BW,'Perimeter');


area=[S.Area];
box=[box.BoundingBox];
perimeter=[boundaries.Perimeter];

peri_area=perimeter./area;
std=0.5;
count=0;
newbox=0;
for i=1:length(peri_area)
    if peri_area(i)>std
        count=count+1;
        newbox(count:count+3)=box(i:i+3);
    end
end
setappdata(0,'box',newbox);
setappdata(0,'peri_area',peri_area);


end

