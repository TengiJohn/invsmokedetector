function smokedec(filename)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
filename='../SmokeDetector/sample/1.avi';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

src=VideoReader(filename);
nFrames=src.NumberOfFrames;
height=src.Height;
width=src.Width;

figure('Name','src');
h1=imshow(zeros(height,width,3));
figure('Name','energy');
h2=imshow(zeros(height/2,width/2),[0,10],'border','tight','initialmagnification','fit');
figure('Name','moving');
h3=imshow(zeros(height,width),[0,1],'border','tight','initialmagnification','fit');

figure('Name','background');
h4=imshow(zeros(height,width,3),'border','tight','initialmagnification','fit');

initial(double(read(src,1)));
gmm_initial(double(read(src,1)));
for k=2:400
    curFrame=double(read(src,k));
    grayFrame=double(rgb2gray(curFrame));
    
    set(h1,'CData',uint8(curFrame));
%     [cA,cH,cV,cD]=dwt2(grayFrame,'haar');
%     set(h2,'CData',cD);
%     set(h2,'CData',wavelet(grayFrame,'haar'));
%     gmm_update(curFrame);
%     gmm_var=getappdata(0,'gmm_var');
%     set(h3,'CData',gmm_var.fore_back);
      bg_model(curFrame);
      set(h3,'CData',getappdata(0,'iteForeground'));
      set(h4,'CData',uint8(getappdata(0,'iteBackground')));
%     color_filter(curFrame);
%     set(h2,'CData',getappdata(0,'colorfilter_image'));
    drawnow;
    
    setappdata(0,'rgbimage',curFrame);
end

end

