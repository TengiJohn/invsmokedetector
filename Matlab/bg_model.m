function bg_model(img)
background=getappdata(0,'iteBackground');
current=img;

T=50;

foreground=abs(rgb2gray(img)-rgb2gray(background))>T;

temp=zeros(size(img,1),size(img,2),3);
temp(:,:,1)=foreground;
temp(:,:,2)=foreground;
temp(:,:,3)=foreground;
a=0.995;
background=a*background+(1-a)*(temp.*background+(1-temp).*current);


setappdata(0,'iteBackground',background);
setappdata(0,'iteForeground',sum(abs(current-background),3)>T);

end

