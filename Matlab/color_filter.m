function color_filter(rgbimage)
diffimage=getappdata(0,'diffForeground');
[x,y]=find(diffimage>0);
%rule 1: R+-alpha=G+-alpha=B+-alpha
alpha=16;

for i=1:length(x)
    rule_1=0;
    R=rgbimage(x(i),y(i),1);
    G=rgbimage(x(i),y(i),2);
    B=rgbimage(x(i),y(i),3);
    if(abs(R-G)<2*alpha)&&(abs(R-B)<2*alpha)&&(abs(G-B)<2*alpha)
        rule_1=1;
    end
    
    rule_23=0;
    intensity=(R+G+B)/3;
    L1=150;
    L2=220;
    D1=80;
    D2=150;
    if(intensity>L1&&intensity<L2)||(intensity>D1&&intensity<D2)
        rule_23=1;
    end
    
    if(rule_23&&rule_1)
        diffimage(x(i),y(i))=1;
    else
        diffimage(x(i),y(i))=0;
    end
end

setappdata(0,'colorfilter_image',diffimage);

end

