function initial(img)
setappdata(0,'rgbimage',img);
setappdata(0,'grayimage',double(rgb2gray(img)));

setappdata(0,'next_to_last_grayimage',double(rgb2gray(img)));

setappdata(0,'diffThreshold',5*ones(size(img,1),size(img,2)));

setappdata(0,'diffBackground',double(rgb2gray(img)));

setappdata(0,'iteBackground',double(img));
end

