function gmm_update(image)

height=size(image,1);
width=size(image,2);
depth=size(image,3);

ini_var=500;
K=5;
alpha=0.02;
beta=0.02;
r=0.02;
var_min=20;
seg_th=0.17;

back_model=2;


gmm_var=getappdata(0,'gmm_var');
means=gmm_var.mean;
var=gmm_var.var;
weight=gmm_var.weight;

%1Ϊǰ����0,Ϊ����
img=zeros(K,width*height,depth);
temp_img=reshape(image,width*height,depth);
for i=1:depth
    img(:,:,i)=ones(K,1)*(temp_img(:,i))';
end

fore_back=ones(height*width,depth);
background=zeros(height*width,depth);


for i=1:depth
        %1 is matched; 0 is unmatched
        temp=abs(img(:,:,i)-means(:,:,i))<2.5*var(:,:,i).^0.5;
        fore_back(:,i)=sum(temp.*weight(:,:,i))<seg_th;
        
        means(:,:,i)=(1-alpha)*means(:,:,i)+alpha*(temp.*img(:,:,i)+(1-temp).*means(:,:,i));
        var(:,:,i)=(1-beta)*var(:,:,i)+beta*(temp.*(means(:,:,i)-img(:,:,i)).^2+(1-temp).*var(:,:,i));
        var(:,:,i)=(var(:,:,i)>var_min).*(var(:,:,i)-var_min)+var_min;
        
        temp1=(cumsum(temp)==1);
        temp_weight=(1-r)*weight(:,:,i)+r*temp1;
        weight(:,:,i)=temp_weight./(ones(K,1)*sum(temp_weight));
        
        affinity=weight(:,:,i)./var(:,:,i);
        
%         [temp2,index]=sort(affinity,'descend');
        [data,index1]=max(affinity);
        [data,index2]=min(weight);
%         background(m,i)=mean(means(index(1:back_model),m,i));
        for p=1:width*height
            background(p,i)=means(index1(p),p,i);
            if(fore_back(p,i))
                means(index2(p),p,i)=img(1,p,i);
                var(index2(p),p,i)=ini_var;
            end
        end
        
        
        
end

fore_back=sum(fore_back,2)>0;
fore_back=reshape(fore_back,height,width);

background=reshape(background,height,width,depth);

gmm_var.mean=means;
gmm_var.var=var;
gmm_var.weight=weight;
gmm_var.fore_back=fore_back;
gmm_var.background=background;

setappdata(0,'gmm_var',gmm_var);
setappdata(0,'foreground',fore_back);
end

