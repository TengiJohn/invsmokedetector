function dwt_fft()
imgname='../SmokeDetector/sample/slpw.jpg';
img=rgb2gray(imread(imgname));
[cA,cH,cV,cD]=dwt2(img,'haar');
temp=fft2(img);
figure
subplot(1,3,1)
imshow(uint8(img));
subplot(1,3,2)
imshow(real(temp),[0,255],'border','tight','initialmagnification','fit');
subplot(1,3,3)
imshow(imresize(cD,2),[0,5],'border','tight','initialmagnification','fit');





end

