function smoke_inflation(filename)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
filename='../SmokeDetector/sample/person_smoke.avi';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

src=VideoReader(filename);
nFrames=src.NumberOfFrames;
height=src.Height;
width=src.Width;

f1=figure('Name','result');

f2=figure('Name','colorfilter');
h2=imshow(zeros(height,width),[0,1]);
f3=figure('Name','moving');
h3=imshow(zeros(height,width),[0,1]);
initial(read(src,1));

% f4=figure('Name','canny');
% h4=imshow(zeros(height,width),[0,1]);
%1141
for k=1:nFrames
    curFrame=read(src,k);
    grayFrame=double(rgb2gray(curFrame));
    
    framediff(grayFrame);
    diffimage=getappdata(0,'diffForeground');
    set(h3,'CData',diffimage);
    
    color_filter(curFrame);
    set(h2,'CData',getappdata(0,'colorfilter_image'));
    disorder();
    
    figure(f1);
    cla
    imshow(curFrame);
    box=getappdata(0,'box');
    for i=1:length(box)/4
       rectangle('Position',box(i*4-3:i*4),'EdgeColor','r');
    end
    
%     set(h4,'CData',edge(grayFrame,'canny'));
    drawnow;
%     w=waitforbuttonpress;
    disp(k);
end
end

