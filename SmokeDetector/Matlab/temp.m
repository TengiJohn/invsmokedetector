function [ output_args ] = temp( input_args )
theta=0:0.1:2*pi+0.1;
R=1;
subplot(3,2,1);
plot(cos(theta),sin(theta),'Color','k');
axis off;
subplot(3,2,3)
rectangle('Position',[-pi/4,-pi/4,pi/2,pi/2],'EdgeColor','k');
axis off;
subplot(3,2,5)
plot(cos(theta),sin(theta),'Color','k');
rectangle('Position',[-pi/4,-pi/4,pi/2,pi/2],'EdgeColor','k');
axis off;

subplot(3,2,2)
plot(cos(theta),sin(theta),'Color','k');
axis off;

subplot(3,2,4)
theta=1:0.1:3;
plot(cos(theta),sin(theta),'Color','k');
hold on
theta=3:0.1:pi*2+1;
plot(cos(theta),sin(theta),'Color','w');
axis off;

subplot(3,2,6)
theta=1:0.1:3;
plot(cos(theta),sin(theta),'Color','w');
hold on
theta=3:0.1:pi*2+1;
plot(cos(theta),sin(theta),'Color','k');
axis off;
end

