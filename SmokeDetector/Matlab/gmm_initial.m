function gmm_initial(firstImg)
%total gaussian distribution number for one pixel
% firstImg=double(imread('D:\Studies\InvSmokeDec\SmokeDetector\sample\slpw.jpg'));
height=size(firstImg,1);
width=size(firstImg,2);
depth=size(firstImg,3);
K=5;

%��ʼ����ֵ
ini_var=500;

gmm_var_mean=zeros(K,width*height,depth);
gmm_var_var=zeros(K,width*height,depth);
gmm_var_weight=zeros(K,width*height,depth);

for i=1:depth
    gmm_var_mean(:,:,i)=ones(K,1)*reshape(firstImg(:,:,i),1,width*height);
    gmm_var_var(:,:,i)=ones(K,1)*ini_var*ones(1,width*height);
    temp=(1:K)/sum(1:K);
    gmm_var_weight(:,:,i)=temp'*ones(1,width*height);
end

gmm_var.mean=gmm_var_mean;
gmm_var.var=gmm_var_var;
gmm_var.weight=gmm_var_weight;

setappdata(0,'gmm_var',gmm_var);

end

