function framediff(img)
previous=getappdata(0,'grayimage');
current=img;

T=getappdata(0,'diffThreshold');
next_to_last_grayimage=getappdata(0,'next_to_last_grayimage');

alpha=0.6;
%运动检测的最小连同区域
minArea=100;

diffimage=abs(current-previous)>T&abs(current-next_to_last_grayimage)>T;

diffBackground=getappdata(0,'diffBackground');

L=bwlabeln(diffimage);

S = regionprops(L,'Area');

BW=ismember(L,find([S.Area]>minArea));

% box=regionprops(BW,'BoundingBox');

foreGround=BW;
% fill out the blob
% for i=1:length(box)
%     rectangle=round(box(i).BoundingBox);
%     for row=rectangle(2):rectangle(2)+rectangle(4)
%         for col=rectangle(1):rectangle(1)+rectangle(3)
%             row=min(row,size(foreGround,1));
%             col=min(col,size(foreGround,2));
%             foreGround(row,col)=abs(current(row,col)-diffBackground(row,col))>T(row,col);
%         end
%     end
% end

% diffBackground=diffBackground*alpha+(1-alpha)*((1-foreGround).*current+foreGround.*diffBackground);
% 
% T=alpha*T+(1-alpha)*(foreGround.*T+5*(1-foreGround).*abs(current-diffBackground));

setappdata(0,'diffThreshold',T);
setappdata(0,'diffForeground',foreGround);
setappdata(0,'next_to_last_grayimage',previous);
setappdata(0,'grayimage',current);
setappdata(0,'diffBackground',diffBackground);

end

