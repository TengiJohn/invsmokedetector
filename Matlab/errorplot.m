function errorplot()
filename=[pwd '\..\SmokeDetector\experiment\sMoky-3.txt'];
a1=load(filename);

filename=[pwd '\..\SmokeDetector\experiment\waste-3.txt'];
a3=load(filename);
a1=[a1;a3];

filename=[pwd '\..\SmokeDetector\experiment\cross-3.txt'];
a2=load(filename);

t=0:0.03:3;
e1=zeros(1,length(t));
e2=zeros(1,length(t));

for i=1:length(t)
    e1(i)=sum(a1(2:2:length(a1))>t(i));
    e2(i)=sum(a2(2:2:length(a2))<t(i));
end

e1=e1/length(a1)/2;
e2=e2/length(a2)/2;

filename=[pwd '\..\SmokeDetector\experiment\sMoky-2.txt'];
a1=load(filename);

filename=[pwd '\..\SmokeDetector\experiment\waste-2.txt'];
a3=load(filename);
a1=[a1;a3];

filename=[pwd '\..\SmokeDetector\experiment\cross-2.txt'];
a2=load(filename);

e11=zeros(1,length(t));
e22=zeros(1,length(t));
for i=1:length(t)
    e11(i)=sum(a1(2:2:length(a1))>t(i));
    e22(i)=sum(a2(2:2:length(a2))<t(i));
end

e11=e11/length(a1)/2;
e22=e22/length(a2)/2;

plot(t,e11,'.r',t,e22,'+r',t,e1,'.b',t,e2,'+b');
hold on;


    
end

